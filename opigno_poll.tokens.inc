<?php

/**
 * @file
 * Builds placeholder tokens for opigno_poll node values.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function opigno_poll_token_info() {
  $node['votes'] = [
    'name' => t("opigno_poll votes"),
    'description' => t("The number of votes that have been cast on a opigno_poll."),
  ];
  $node['winner'] = [
    'name' => t("opigno_poll winner"),
    'description' => t("The winning opigno_poll answer."),
  ];
  $node['winner-votes'] = [
    'name' => t("opigno_poll winner votes"),
    'description' => t("The number of votes received by the winning opigno_poll answer."),
  ];
  $node['winner-percent'] = [
    'name' => t("opigno_poll winner percent"),
    'description' => t("The percentage of votes received by the winning opigno_poll answer."),
  ];
  $node['duration'] = [
    'name' => t("opigno_poll duration"),
    'description' => t("The length of time the opigno_poll is set to run."),
  ];

  return [
    'tokens' => ['opigno_poll' => $node],
  ];
}

/**
 * Implements hook_tokens().
 */
function opigno_poll_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type == 'opigno_poll' && !empty($data['opigno_poll'])) {
    /**
* @var \Drupal\opigno_poll\Entity\Poll $opigno_poll
*/
    $opigno_poll = $data['opigno_poll'];

    $total_votes = 0;
    $highest_votes = 0;
    foreach ($opigno_poll->getVotes() as $vote) {
      $total_votes += $vote;
    }
    $options = $opigno_poll->getOptions();
    foreach ($opigno_poll->getVotes() as $pid => $vote) {
      if ($vote > $highest_votes) {
        $winner = $options[$pid];
        $highest_votes = $vote;
      }
    }
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'votes':
          $replacements[$original] = $total_votes;
          break;

        case 'winner':
          if (isset($winner)) {
            $replacements[$original] = $winner;
          }
          else {
            $replacements[$original] = '';
          }
          break;

        case 'winner-votes':
          if (isset($winner)) {
            $replacements[$original] = $highest_votes;
          }
          else {
            $replacements[$original] = '';
          }
          break;

        case 'winner-percent':
          if (isset($winner)) {
            $percent = ($highest_votes / $total_votes) * 100;
            $replacements[$original] = number_format($percent, 0);
          }
          else {
            $replacements[$original] = '';
          }
          break;

        case 'duration':
          $replacements[$original] = \Drupal::service('date.formatter')->formatInterval($opigno_poll->getRuntime());
          break;
      }
    }
  }

  return $replacements;
}
