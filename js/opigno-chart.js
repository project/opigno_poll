(function ($, Drupal) {
    Drupal.behaviors.opignopoll = {
        attach: function (context, settings) {
            // Your JavaScript code for Google Charts.
            google.charts.load('current', {'packages':['bar']});
            google.charts.setOnLoadCallback(drawStuff);

            function drawStuff()
            {
                var data = drupalSettings.opignopoll.chartData;

                // Add header for the data
                var chartData = [
                ['Move', 'Percentage'],
                ];

                // Add each data row
                data.forEach(
                    function (item) {
                        chartData.push([item[0], item[1]]);
                    }
                );

                // Use chartData for your chart
                console.log(chartData);

                var data = new google.visualization.arrayToDataTable(chartData);

                var options = {
                    width: 800,
                    legend: { position: 'none' },
                    chart: {
                        title: 'Chess opening moves',
                        subtitle: 'popularity by percentage'
                    },
                    axes: {
                        x: {
                            0: { side: 'top', label: 'White to move'} // Top x-axis.
                        }
                    },
                    bar: { groupWidth: "90%" }
                };

                var chart = new google.charts.Bar(document.getElementById('top_x_div'));
                // Convert the Classic options to Material options.
                chart.draw(data, google.charts.Bar.convertOptions(options));
            }
        }
    };
})(jQuery, Drupal);
