(function ($, Drupal, once) {

    $('.opigno_poll-existing-choice').on(
        'focus', function (event) {
            $(once('opigno_poll-existing-choice'), document).each(
                function () {
                    $(Drupal.theme('opigno_pollChoiceDeleteWarning')).insertBefore($('#choice-values')).hide().fadeIn('slow');
                }
            );
        }
    );

    $.extend(
        Drupal.theme, /**
        * @lends Drupal.theme
                       */{

        /**
         * @return {string}
         *   Markup for the warning.
         */
    opigno_pollChoiceDeleteWarning: function () {
        return '<div class="messages messages--warning" role="alert">' + Drupal.t('* Deleting a choice will delete the votes on it!') + '</div>';
    }
        }
    );

})(jQuery, Drupal, once);
