opigno_poll (from core) module for Drupal 8
====================================

Implementation of the opigno_poll module that was removed from Drupal 8 core. opigno_poll is an entity with a opigno_poll Choice field type to encapsulate each choice option and the initial vote. 
