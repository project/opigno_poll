<?php

namespace Drupal\opigno_poll\Plugin\migrate\source;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Gets the opigno_poll choices data from the source database.
 *
 * @MigrateSource(
 *   id = "opigno_poll_choice",
 *   source_module = "opigno_poll"
 * )
 */
class PollChoice extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Select opigno_poll choices.
    $query = $this->select('opigno_poll_choice', 'pc')
      ->fields('pc');
    $query->orderBy('chid', 'ASC');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'chid' => $this->t('Unique identifier of a opigno_poll choice.'),
      'chtext' => $this->t('Text of the opigno_poll option'),
      'weight' => $this->t('The sort order of this choice among all choices'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['chid']['type'] = 'integer';
    return $ids;
  }

}
