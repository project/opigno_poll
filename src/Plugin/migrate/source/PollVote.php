<?php

namespace Drupal\opigno_poll\Plugin\migrate\source;

use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Gets the opigno_poll votes data from the source database.
 *
 * @MigrateSource(
 *   id = "opigno_poll_vote",
 *   source_module = "opigno_poll"
 * )
 */
class PollVote extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Select opigno_poll in its last revision.
    $query = $this->select('opigno_poll_vote', 'pv')
      ->fields('pv')
      ->orderBy('chid', 'ASC');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'chid' => $this->t("The user's vote for this opigno_poll"),
      'uid' => $this->t('user ID for authenticated user'),
      'nid' => $this->t('user Node ID that this vote was cast on'),
      'hostname' => $this->t('The ip address this vote is from.'),
      'timestamp' => $this->t('The timestamp of the vote creation.'),
    ];
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['chid']['type'] = 'integer';
    $ids['uid']['type'] = 'integer';
    $ids['timestamp']['type'] = 'integer';
    return $ids;
  }

}
