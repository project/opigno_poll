<?php

namespace Drupal\opigno_poll\Plugin\migrate\destination;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Row;
use Drupal\opigno_poll\PollVoteStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Destination plugin to migrate opigno_poll votes.
 *
 * @MigrateDestination(
 *   id = "opigno_poll_vote"
 * )
 */
class PollVote extends DestinationBase implements ContainerFactoryPluginInterface {

  /**
   * The opigno_poll vote storage service.
   *
   * @var \Drupal\opigno_poll\PollVoteStorageInterface
   */
  protected $PollVoteStorage;

  /**
   * Constructs an entity destination plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The migration.
   * @param \Drupal\opigno_poll\PollVoteStorageInterface $PollVoteStorage
   *   The opigno_poll vote storage service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, PollVoteStorageInterface $PollVoteStorage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->PollVoteStorage = $PollVoteStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('opigno_poll_vote.storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {
    $vote = [];
    $vote['chid'] = $row->getDestinationProperty('chid');
    $vote['pid'] = $row->getDestinationProperty('pid');
    $vote['uid'] = $row->getDestinationProperty('uid');
    $vote['hostname'] = $row->getDestinationProperty('hostname');
    $vote['timestamp'] = $row->getDestinationProperty('timestamp');

    $this->PollVoteStorage->saveVote($vote);
    return [$vote['chid'], $vote['uid'], $vote['timestamp']];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['chid']['type'] = 'integer';
    $ids['uid']['type'] = 'integer';
    $ids['timestamp']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    $fields = [
      'chid' => $this->t("The user's vote for this opigno_poll"),
      'uid' => $this->t('user ID for authenticated user'),
      'pid' => $this->t('user opigno_poll ID that this vote was cast on'),
      'hostname' => $this->t('The ip address this vote is from.'),
      'timestamp' => $this->t('The timestamp of the vote creation.'),
    ];
    return $fields;
  }

}
