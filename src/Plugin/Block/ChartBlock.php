<?php

namespace Drupal\opigno_poll\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\opigno_poll\Entity\Poll;
use Drupal\opigno_poll\PollInterface;

/**
 * Provides a 'article' block.
 *
 * @Block(
 *   id = "chart_block",
 *   admin_label = @Translation("Chart block"),
 *   category = @Translation("Custom article block example")
 * )
 */
class ChartBlock extends BlockBase {
  /**
   * The opignopoll of the form.
   *
   * @var \Drupal\opign_opoll\PollInterface
   */
  protected $opignopoll;

  /**
   * {@inheritdoc}
   */
  public function build() {
    $poll_entity = \Drupal::request()->attributes->get('opigno_poll');
    if (!$poll_entity) {
      $poll_entity = Poll::load(\Drupal::request()->attributes->get('entity_id'));
    }
    $view_mode = 'default';
    // Call the service to show poll results.
    $results = \Drupal::service('opigno_poll_vote.storage')->showopignopollResults($poll_entity, $view_mode);
    // Initialize the $data array.
    $data = [];

    foreach ($results['#results'] as $key => $item) {
      // Get the percentage and choice from each item.
      $percentage = $item['#percentage'];
      $choice = $item['#choice'];

      // Add the percentage and choice to the $data array.
      $data[] = [$choice, $percentage];
    }
    $build = [
      '#theme' => 'block_chart',
    ];
    $build['#attached']['drupalSettings']['opignopoll']['chartData'] = $data;
    $build['#attached']['library'][] = 'opigno_poll/drupal.opignopoll-links';
    return $build;

  }

  /**
   * {@inheritdoc}
   */
  public function shopollResults(PollInterface $opignopoll, $view_mode = 'default', $block = FALSE) {

    // Ensure that a page that shows opignopoll results can not be cached.
    \Drupal::service('page_cache_kill_switch')->trigger();
    $total_votes = 0;
    foreach ($opignopoll->getVotes() as $vote) {
      $total_votes += $vote;
    }

    $options = $opignopoll->getOptions();
    $opignopoll_results = [];
    foreach ($opignopoll->getVotes() as $pid => $vote) {
      $percentage = round($vote * 100 / max($total_votes, 1));
      $display_votes = (!$block) ? ' (' . \Drupal::translation()
        ->formatPlural($vote, '1 vote', '@count votes') . ')' : '';

      $opignopoll_results[] = [
        '#theme' => 'opigno_poll_meter',
        '#choice' => $options[$pid],
        '#display_value' => t('@percentage%', ['@percentage' => $percentage]) . $display_votes,
        '#min' => 0,
        '#max' => $total_votes,
        '#value' => $vote,
        '#percentage' => $percentage,
        '#attributes' => ['class' => ['bar']],
        '#opigno_poll' => $opignopoll,
      ];
    }
  }

}
