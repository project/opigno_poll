<?php

namespace Drupal\opigno_poll\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to display the flag indicating opigno_poll activity.
 *
 * @ViewsField("opigno_poll_status")
 */
class PollStatus extends FieldPluginBase {

  /**
   * Processes values from a views result row.
   *
   * @param \Drupal\views\ResultRow $values
   *   The values from the views result row.
   *
   * @return mixed
   *   Returns the processed result.
   */
  public function render(ResultRow $values) {
    /**
* @var \Drupal\opigno_poll\PollInterface $entity
*/
    $entity = $values->_entity;

    if ($entity->isOpen() && $entity->getRuntime() != 0) {
      $date = \Drupal::service('date.formatter')->format($entity->getCreated() + $entity->getRuntime(), 'short');
      $output = 'Yes (until ' . rtrim(strstr($date, '-', TRUE)) . ')';
    }
    elseif ($entity->isOpen()) {
      $output = t('Yes');
    }
    else {
      $output = 'No';
    }

    return $output;
  }

}
