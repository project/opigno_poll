<?php

namespace Drupal\opigno_poll\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler which shows the total votes for a opigno_poll.
 *
 * @ViewsField("opigno_poll_totalvotes")
 */
class PollTotalVotes extends FieldPluginBase {

  /**
   * Process values from a views result row.
   *
   * @param \Drupal\views\ResultRow $values
   *   The values from the views result row.
   *
   * @return mixed
   *   The processed result.
   */
  public function render(ResultRow $values) {
    /**
* @var \Drupal\opigno_poll\PollVoteStorage $vote_storage
*/
    $vote_storage = \Drupal::service('opigno_poll_vote.storage');
    $entity = opigno_poll::load($this->getValue($values));
    $build['#markup'] = $vote_storage->getTotalVotes($entity);
    $build['#cache']['tags'][] = 'opigno_poll-votes:' . $entity->id();
    return $build;
  }

}
