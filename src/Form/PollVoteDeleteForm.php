<?php

namespace Drupal\opigno_poll\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Provides a form for deleting a vote.
 */
class PollVoteDeleteForm extends ContentEntityConfirmFormBase implements ContainerAwareInterface {
  use ContainerAwareTrait;

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this vote for %opigno_poll', ['%opigno_poll' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->entity->toUrl();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uid = $this->container->get('request_stack')->getCurrentRequest()->attributes->get('user');
    $account = User::load($uid);
    /**
* @var \Drupal\opigno_poll\PollVoteStorage $vote_storage
*/
    $vote_storage = \Drupal::service('opigno_poll_vote.storage');
    $vote_storage->cancelVote($this->entity, $account);
    \Drupal::logger('opigno_poll')->notice(
          '%user\'s vote in opigno_poll #%opigno_poll deleted.', [
            '%user' => $account->id(),
            '%opigno_poll' => $this->entity->id(),
          ]
      );
    $this->messenger()->addMessage($this->t('Your vote was cancelled.'));

    // Display the original opigno_poll.
    $form_state->setRedirect('entity.opigno_poll.canonical', ['opigno_poll' => $this->entity->id()]);
  }

}
