<?php

namespace Drupal\opigno_poll\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting a opigno_poll.
 */
class PollDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return t('All associated votes will be deleted too. This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this opigno_poll %opigno_poll', ['%opigno_poll' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('opigno_poll.opigno_poll_list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    \Drupal::logger('opigno_poll')->notice('opigno_poll %opigno_poll deleted.', ['%opigno_poll' => $this->entity->label()]);
    $this->messenger()->addMessage($this->t('The opigno_poll %opigno_poll has been deleted.', ['%opigno_poll' => $this->entity->label()]));
    $form_state->setRedirect('opigno_poll.opigno_poll_list');
  }

}
