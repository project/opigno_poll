<?php

namespace Drupal\opigno_poll\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the opigno_poll edit forms.
 */
class PollForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $opigno_poll = $this->entity;

    if ($opigno_poll->isNew()) {
      $title = $this->t('Add new opigno_poll');
    }
    else {
      $title = $this->t('Edit @label', ['@label' => $opigno_poll->label()]);
    }
    $form['#title'] = $title;

    foreach ($form['choice']['widget'] as $key => $choice) {
      if (is_int($key) && $form['choice']['widget'][$key]['choice']['#default_value'] != NULL) {
        $form['choice']['widget'][$key]['choice']['#attributes'] = ['class' => ['opigno_poll-existing-choice']];
      }
    }

    $form['#attached'] = ['library' => ['opigno_poll/admin']];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $opigno_poll = $this->buildEntity($form, $form_state);
    // Check for duplicate titles.
    $opigno_poll_storage = $this->entityTypeManager->getStorage('opigno_poll');
    $result = $opigno_poll_storage->getOpignoPollDuplicates($opigno_poll);
    foreach ($result as $item) {
      if (strcasecmp($item->label(), $opigno_poll->label()) == 0) {
        $form_state->setErrorByName('question', $this->t('A feed named %feed already exists. Enter a unique question.', ['%feed' => $opigno_poll->label()]));
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $opigno_poll = $this->entity;
    $insert = (bool) $opigno_poll->id();
    $opigno_poll->save();
    if ($insert) {
      $this->messenger()->addMessage($this->t('The opigno_poll %opigno_poll has been updated.', ['%opigno_poll' => $opigno_poll->label()]));
    }
    else {
      \Drupal::logger('opigno_poll')->notice(
        'opigno_poll %opigno_poll added.',
        ['%opigno_poll' => $opigno_poll->label(), 'link' => $opigno_poll->toLink()->toString()]
      );
      $this->messenger()->addMessage($this->t('The opigno_poll %opigno_poll has been added.', ['%opigno_poll' => $opigno_poll->label()]));
    }

    $form_state->setRedirect('opigno_poll.opigno_poll_list');
  }

}
