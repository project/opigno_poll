<?php

namespace Drupal\opigno_poll\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\opigno_poll\PollInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays banned IP addresses.
 */
class PollViewForm extends FormBase implements BaseFormIdInterface {

  /**
   * The opigno_poll of the form.
   *
   * @var \Drupal\opigno_poll\PollInterface
   */
  protected $opignoPoll;

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'opigno_poll_view_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'opigno_poll_view_form_' . $this->opigno_poll->id();
  }

  /**
   * Set the opigno_poll of this form.
   *
   * @param \Drupal\opigno_poll\PollInterface $opignoPoll
   *   The opigno_poll that will be set in the form.
   */
  public function setOpignoPoll(PollInterface $opignoPoll) {
    $this->opigno_poll = $opignoPoll;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL, $view_mode = 'full') {
    // Add the opigno_poll to the form.
    $form['opigno_poll']['#type'] = 'value';
    $form['opigno_poll']['#value'] = $this->opigno_poll;
    $form['#view_mode'] = $view_mode;

    if ($this->showResults($this->opigno_poll, $form_state)) {

      // Check if the user already voted. The form is still being built but
      // the Vote button won't be added so the submit callbacks will not be
      // called. Directly check for the request method and use the raw user
      // input.
      if ($request->isMethod('POST') && $this->opigno_poll->hasUserVoted()) {
        $input = $form_state->getUserInput();
        if (isset($input['op']) && $input['op'] == $this->t('Vote')) {
          $this->messenger()->addError($this->t('Your vote for this opigno_poll has already been submitted.'));
          $_SESSION['opigno_poll_vote'][$this->opigno_poll->id()] = FALSE;
        }
      }

      $form['results'] = \Drupal::service('opigno_poll_vote.storage')->showopignopollResults($this->opigno_poll, $view_mode);

      // For all view modes except full and block (as block displays it as the
      // block title), display the question.
      if ($view_mode != 'full' && $view_mode != 'block') {
        $form['results']['#show_question'] = TRUE;
      }
    }
    else {
      $options = $this->opigno_poll->getOptions();
      if ($options) {
        $form['choice'] = [
          '#type' => 'radios',
          '#title' => t('Choices'),
          '#title_display' => 'invisible',
          '#options' => $options,
        ];
      }
      $form['#theme'] = 'opigno_poll_vote';
      $form['#entity'] = $this->opigno_poll;
      $form['#action'] = $this->opigno_poll->toUrl()->setOption('query', \Drupal::destination()->getAsArray())->toString();
      // Set a flag to hide results which will be removed if we want to view
      // results when the form is rebuilt.
      $form_state->set('show_results', FALSE);

      // For all view modes except full and block (as block displays it as the
      // block title), display the question.
      if ($view_mode != 'full' && $view_mode != 'block') {
        $form['#show_question'] = TRUE;
      }

    }

    $form['actions'] = $this->actions($form, $form_state, $this->opigno_poll);

    $form['#cache'] = [
      'tags' => $this->opigno_poll->getCacheTags(),
    ];

    return $form;
  }

  /**
   * Ajax callback to replace the opigno_poll form.
   */
  public function ajaxReplaceForm(array $form, FormStateInterface $form_state) {
    // Embed status message into the form.
    $form = ['messages' => ['#type' => 'status_messages']] + $form;
    /**
* @var \Drupal\Core\Render\RendererInterface $renderer
*/
    $renderer = \Drupal::service('renderer');
    // Render the form.
    $output = $renderer->renderRoot($form);

    $response = new AjaxResponse();
    $response->setAttachments($form['#attached']);

    // Replace the form completely and return it.
    return $response->addCommand(new ReplaceCommand('.opigno-poll-view-form-' . $this->opigno_poll->id(), $output));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function showResults(PollInterface $opignoPoll, FormStateInterface $form_state) {
    $account = $this->currentUser();
    switch (TRUE) {
      // The "View results" button, when available, has been clicked.
      case $form_state->get('show_results'):
        return TRUE;

      // The opigno_poll is closed.
      case ($opignoPoll->isClosed()):
        return TRUE;

      // Unauthorized user attempting to view inaccessible opigno_poll.
      case ($account->isAnonymous() && !$opignoPoll->getAnonymousVoteAllow()):
        return TRUE;

      // The user has already voted.
      case ($opignoPoll->hasUserVoted()):
        return TRUE;

      default:
        return FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state, $opignoPoll) {
    $actions = [];

    // Requires that we explicitly provide the ajax_form query argument too in
    // the separate options key, as that replaces all options of the Url object.
    $ajax = [
      'callback' => '::ajaxReplaceForm',
      'url' => $this->opigno_poll->toUrl(),
      'options' => ['query' => [FormBuilderInterface::AJAX_FORM_REQUEST => TRUE, 'view_mode' => $form['#view_mode']]],
    ];

    if ($this->showResults($opignoPoll, $form_state)) {
      // Allow user to cancel their vote.
      if ($this->isCancelAllowed($opignoPoll)) {
        $actions['#type'] = 'actions';
        $actions['cancel']['#type'] = 'submit';
        $actions['cancel']['#button_type'] = 'primary';
        $actions['cancel']['#value'] = t('Cancel vote');
        $actions['cancel']['#submit'] = ['::cancel'];
        $actions['cancel']['#ajax'] = $ajax;
        $actions['cancel']['#weight'] = '0';
      }
      if (!$opignoPoll->hasUserVoted() && $opignoPoll->isOpen() && $opignoPoll->getAnonymousVoteAllow()) {
        $actions['#type'] = 'actions';
        $actions['back']['#type'] = 'submit';
        $actions['back']['#button_type'] = 'primary';
        $actions['back']['#value'] = t('View opigno_poll');
        $actions['back']['#submit'] = ['::back'];
        $actions['back']['#ajax'] = $ajax;
        $actions['back']['#weight'] = '0';
      }
    }
    else {
      $actions['#type'] = 'actions';
      $actions['vote']['#type'] = 'submit';
      $actions['vote']['#button_type'] = 'primary';
      $actions['vote']['#value'] = t('Vote');
      $actions['vote']['#validate'] = ['::validateVote'];
      $actions['vote']['#submit'] = ['::save'];
      $actions['vote']['#ajax'] = $ajax;
      $actions['vote']['#weight'] = '0';

      // View results before voting.
      if ($opignoPoll->result_vote_allow->value || $this->currentUser()->hasPermission('view opigno_poll results')) {
        $actions['result']['#type'] = 'submit';
        $actions['result']['#button_type'] = 'primary';
        $actions['result']['#value'] = t('View results');
        $actions['result']['#submit'] = ['::result'];
        $actions['result']['#ajax'] = $ajax;
        $actions['result']['#weight'] = '1';
      }
    }

    return $actions;
  }

  /**
   * Cancel vote submit function.
   *
   * @param array $form
   *   The previous form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function cancel(array $form, FormStateInterface $form_state) {
    /**
* @var \Drupal\opigno_poll\PollVoteStorageInterface $vote_storage
*/
    $vote_storage = \Drupal::service('opigno_poll_vote.storage');
    $vote_storage->cancelVote($this->opigno_poll, $this->currentUser());
    \Drupal::logger('opigno_poll')->notice(
          '%user\'s vote in opigno_poll #%opigno_poll deleted.', [
            '%user' => $this->currentUser()->id(),
            '%opigno_poll' => $this->opigno_poll->id(),
          ]
      );
    $this->messenger()->addMessage($this->t('Your vote was cancelled.'));

    // In case of an ajax submission, trigger a form rebuild so that we can
    // return an updated form through the ajax callback.
    if ($this->getRequest()->query->get('ajax_form')) {
      $form_state->setRebuild(TRUE);
    }
  }

  /**
   * View vote results submit function.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function result(array $form, FormStateInterface $form_state) {
    $form_state->set('show_results', TRUE);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Back to opigno_poll view submit function.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function back(array $form, FormStateInterface $form_state) {
    $form_state->set('show_results', FALSE);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Save a user's vote submit function.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function save(array $form, FormStateInterface $form_state) {
    $options = [];
    $options['chid'] = $form_state->getValue('choice');
    $options['uid'] = $this->currentUser()->id();
    $options['pid'] = $form_state->getValue('opigno_poll')->id();
    $options['hostname'] = \Drupal::request()->getClientIp();
    $options['timestamp'] = \Drupal::time()->getRequestTime();
    // Save vote.
    /**
* @var \Drupal\opigno_poll\PollVoteStorage $vote_storage
*/
    $vote_storage = \Drupal::service('opigno_poll_vote.storage');
    $vote_storage->saveVote($options);
    $this->messenger()->addMessage($this->t('Your vote has been recorded.'));

    if ($this->currentUser()->isAnonymous()) {
      // The vote is recorded so the user gets the result view instead of the
      // convenient side effect of preventing the user from hitting the page
      // cache. When anonymous voting is allowed, the page cache should only
      // contain the voting form, not the results.
      $_SESSION['opigno_poll_vote'][$form_state->getValue('opigno_poll')->id()] = $form_state->getValue('choice');
    }

    // In case of an ajax submission, trigger a form rebuild so that we can
    // return an updated form through the ajax callback.
    if ($this->getRequest()->query->get('ajax_form')) {
      $form_state->setRebuild(TRUE);
    }

    // No explicit redirect, so that we stay on the current page, which might
    // Whether the opigno_poll is displayed on its form or another page.
    // example as a block.
  }

  /**
   * Validates the vote action.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateVote(array &$form, FormStateInterface $form_state) {
    if (!$form_state->hasValue('choice')) {
      $form_state->setErrorByName('choice', $this->t('Your vote could not be recorded because you did not select any of the choices.'));
    }
  }

  /**
   * Checks if the current user is allowed to cancel on the given opigno_poll.
   *
   * @param \Drupal\opigno_poll\PollInterface $opignoPoll
   *   The opigno_poll to check.
   *
   * @return bool
   *   TRUE if the user can cancel.
   */
  protected function isCancelAllowed(PollInterface $opignoPoll) {
    // Allow access if the user has voted.
    return $opignoPoll->hasUserVoted()
        // And the opigno_poll allows to cancel votes.
        && $opignoPoll->getCancelVoteAllow()
        // And the user has the cancel own vote permission.
        && (\Drupal::currentUser()->isAuthenticated() || !empty($_SESSION['opigno_poll_vote'][$opignoPoll->id()]))
        // And opigno_poll is open.
        && $opignoPoll->isOpen();
  }

}
