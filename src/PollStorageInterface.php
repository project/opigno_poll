<?php

namespace Drupal\opigno_poll;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines a common interface for opigno_poll entity controller classes.
 */
interface PollStorageInterface extends EntityStorageInterface {

  /**
   * Save a user's vote.
   *
   * @param array $options
   *   An array of options for saving the user's vote.
   *   Example: ['user_id' => 123, 'vote' => 'option1'].
   *
   * @return mixed
   *   Returns the result of the vote saving operation.
   */
  public function saveVote(array $options);

  /**
   * Cancel a user's vote.
   *
   * @param PollInterface $opigno_poll
   *   The poll to cancel the user's vote from.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account whose vote will be canceled.
   *
   * @return mixed
   *   Returns the result of the vote cancellation operation.
   */
  public function cancelVote(PollInterface $opigno_poll, AccountInterface $account = NULL);

  /**
   * Get total votes for an opigno_poll.
   *
   * @param PollInterface $opigno_poll
   *   The opigno_poll for which to get total votes.
   *
   * @return mixed
   *   Returns the total number of votes for the opigno_poll.
   */
  public function getTotalVotes(PollInterface $opigno_poll);

  /**
   * Get all votes for an opigno_poll.
   *
   * @param PollInterface $opigno_poll
   *   The opigno_poll for which to get all votes.
   *
   * @return mixed
   *   Returns the array of all votes for the opigno_poll.
   */
  public function getVotes(PollInterface $opigno_poll);

  /**
   * Delete a user's votes for an opigno_poll.
   *
   * @param PollInterface $opigno_poll
   *   The opigno_poll for which to delete the user's votes.
   *
   * @return mixed
   *   Returns the result of the vote deletion operation.
   */
  public function deleteVotes(PollInterface $opigno_poll);

  /**
   * Get a user's votes for an opigno_poll.
   *
   * @param PollInterface $opigno_poll
   *   The opigno_poll for which to get the user's votes.
   *
   * @return mixed
   *   Returns the user's votes for the opigno_poll.
   */
  public function getUserVote(PollInterface $opigno_poll);

  /**
   * Get the most recent opigno_poll posted on the site.
   *
   * @return mixed
   *   Returns the most recent opigno_poll object.
   */
  public function getMostRecentopignopoll();

  /**
   * Find all duplicates of an opigno_poll by matching the question.
   *
   * @param PollInterface $opigno_poll
   *   The opigno_poll to find duplicates of.
   *
   * @return mixed
   *   Returns an array containing all duplicates of the opigno_poll.
   */
  public function getOpignoPollDuplicates(PollInterface $opigno_poll);

  /**
   * Returns all expired opigno_polls.
   *
   * @return \Drupal\opigno_poll\PollInterface[]
   *   An array of expired opigno_poll objects.
   */
  public function getExpiredopignopolls();

}
