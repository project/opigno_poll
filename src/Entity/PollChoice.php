<?php

namespace Drupal\opigno_poll\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\opigno_poll\PollChoiceInterface;

/**
 * Defines the opigno_poll choice entity class.
 *
 * @ContentEntityType(
 *   id = "opigno_poll_choice",
 *   label = @Translation("opigno_poll Choice"),
 *   base_table = "opigno_poll_choice",
 *   data_table = "opigno_poll_choice_field_data",
 *   admin_permission = "administer opigno_polls",
 *   content_translation_ui_skip = TRUE,
 *   translatable = TRUE,
 *   content_translation_metadata = "Drupal\opigno_poll\PollChoiceTranslationMetadataWrapper",
 *   handlers = {
 *     "translation" = "Drupal\opigno_poll\PollChoiceTranslationHandler",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "choice",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode"
 *   }
 * )
 */
class PollChoice extends ContentEntityBase implements PollChoiceInterface {

  /**
   * Flag indicating whether the task is complete.
   *
   * @var bool
   */
  protected $needsSave = NULL;

  /**
   * {@inheritdoc}
   */
  public function setChoice($question) {
    $this->set('choice', $question);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function needsSaving($new_value = NULL) {
    // If explicitly set, return that value. otherwise fall back to isNew(),
    // saving is always required for new entities.
    $return = $this->needsSave ?? $this->isNew();

    if ($new_value !== NULL) {
      $this->needsSave = $new_value;
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Choice ID'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setReadOnly(TRUE);

    $fields['choice'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Choice'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions(
              'form', [
                'type' => 'string_textfield',
                'weight' => -100,
              ]
          );

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The opigno_poll language code.'));

    return $fields;
  }

}
