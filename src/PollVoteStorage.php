<?php

namespace Drupal\opigno_poll;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;

/**
 * Controller class for opigno_poll vote storage.
 */
class PollVoteStorage implements PollVoteStorageInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Opigno_poll vote for current user, indexed by opigno_poll and User ID.
   *
   * @var array[]
   */
  protected $currentUserVote = [];

  /**
   * Constructs a new PollVoteStorage.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   A Database connection to use for reading and writing database data.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator.
   */
  public function __construct(Connection $connection, CacheTagsInvalidatorInterface $cache_tags_invalidator) {
    $this->connection = $connection;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteChoicesVotes(array $choices) {
    $this->connection->delete('opigno_poll_vote')
      ->condition('chid', $choices, 'IN')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVotes(PollInterface $opigno_poll) {
    $this->connection->delete('opigno_poll_vote')->condition('pid', $opigno_poll->id())
      ->execute();

    // Deleting a vote means that any cached vote might not be updated in the
    // UI, so we need to invalidate them all.
    $this->cacheTagsInvalidator->invalidateTags(['opigno_poll-votes:' . $opigno_poll->id()]);
    // Invalidate the static cache of votes.
    $this->currentUserVote = [];
  }

  /**
   * {@inheritdoc}
   */
  public function cancelVote(PollInterface $opigno_poll, AccountInterface $account = NULL) {
    if ($account->id()) {
      $this->connection->delete('opigno_poll_vote')
        ->condition('pid', $opigno_poll->id())
        ->condition('uid', $account->id())
        ->execute();
    }
    else {
      $this->connection->delete('opigno_poll_vote')
        ->condition('pid', $opigno_poll->id())
        ->condition('uid', \Drupal::currentUser()->id())
        ->condition('hostname', \Drupal::request()->getClientIp())
        ->execute();
    }

    // Deleting a vote means that any cached vote might not be updated in the
    // UI, so we need to invalidate them all.
    $this->cacheTagsInvalidator->invalidateTags(['opigno_poll-votes:' . $opigno_poll->id()]);
    // Invalidate the static cache of votes.
    $this->currentUserVote = [];
  }

  /**
   * {@inheritdoc}
   */
  public function saveVote(array $options) {
    if (!is_array($options)) {
      return;
    }
    $this->connection->insert('opigno_poll_vote')->fields($options)->execute();

    // Deleting a vote means that any cached vote might not be updated in the
    // UI, so we need to invalidate them all.
    $this->cacheTagsInvalidator->invalidateTags(['opigno_poll-votes:' . $options['pid']]);
    // Invalidate the static cache of votes.
    $this->currentUserVote = [];
  }

  /**
   * {@inheritdoc}
   */
  public function getVotes(PollInterface $opigno_poll) {
    $votes = [];
    // Set votes for all options to 0.
    $options = $opigno_poll->getOptions();
    foreach ($options as $id => $label) {
      $votes[$id] = 0;
    }

    $result = $this->connection->query("SELECT chid, COUNT(chid) AS votes FROM {opigno_poll_vote} WHERE pid = :pid GROUP BY chid", [':pid' => $opigno_poll->id()]);
    // Replace the count for options that have recorded votes in the database.
    foreach ($result as $row) {
      $votes[$row->chid] = $row->votes;
    }

    return $votes;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserVote(PollInterface $opigno_poll) {
    $uid = \Drupal::currentUser()->id();
    $key = $opigno_poll->id() . ':' . $uid;
    if (isset($this->currentUserVote[$key])) {
      return $this->currentUserVote[$key];
    }
    $this->currentUserVote[$key] = FALSE;
    if ($uid || $opigno_poll->getAnonymousVoteAllow()) {
      if ($uid) {
        $query = $this->connection->query(
              "SELECT * FROM {opigno_poll_vote} WHERE pid = :pid AND uid = :uid", [
                ':pid' => $opigno_poll->id(),
                ':uid' => $uid,
              ]
          );
      }
      else {
        $query = $this->connection->query(
              "SELECT * FROM {opigno_poll_vote} WHERE pid = :pid AND hostname = :hostname AND uid = 0", [
                ':pid' => $opigno_poll->id(),
                ':hostname' => \Drupal::request()->getClientIp(),
              ]
          );
      }
      $this->currentUserVote[$key] = $query->fetchAssoc();
    }
    return $this->currentUserVote[$key];
  }

  /**
   * {@inheritdoc}
   */
  public function getTotalVotes(PollInterface $opigno_poll) {
    $query = $this->connection->query("SELECT COUNT(chid) FROM {opigno_poll_vote} WHERE pid = :pid", [':pid' => $opigno_poll->id()]);
    return $query->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function showopignopollResults(PollInterface $opignopoll, $view_mode = 'default', $block = FALSE) {

    // Ensure that a page that shows opignopoll results can not be cached.
    \Drupal::service('page_cache_kill_switch')->trigger();

    $total_votes = 0;
    foreach ($opignopoll->getVotes() as $vote) {
      $total_votes += $vote;
    }

    $options = $opignopoll->getOptions();
    $opignopoll_results = [];
    foreach ($opignopoll->getVotes() as $pid => $vote) {
      $percentage = round($vote * 100 / max($total_votes, 1));
      $display_votes = (!$block) ? ' (' . \Drupal::translation()
        ->formatPlural($vote, '1 vote', '@count votes') . ')' : '';

      $opignopoll_results[] = [
        '#theme' => 'opigno_poll_meter',
        '#choice' => $options[$pid],
        '#display_value' => t('@percentage%', ['@percentage' => $percentage]) . $display_votes,
        '#min' => 0,
        '#max' => $total_votes,
        '#value' => $vote,
        '#percentage' => $percentage,
        '#attributes' => ['class' => ['bar']],
        '#opignopoll' => $opignopoll,
      ];
    }

    /**
* @var \Drupal\opignopoll\PollVoteStorageInterface $vote_storage
*/
    $vote_storage = \Drupal::service('opigno_poll_vote.storage');
    $user_vote = $vote_storage->getUserVote($opignopoll);

    $output = [
      '#theme' => 'opigno_poll_results',
      '#raw_question' => $opignopoll->label(),
      '#results' => $opignopoll_results,
      '#votes' => $total_votes,
      '#block' => $block,
      '#pid' => $opignopoll->id(),
      '#opignopoll' => $opignopoll,
      '#view_mode' => $view_mode,
      '#vote' => $user_vote['chid'] ?? NULL,
    ];

    return $output;
  }

}
