<?php

namespace Drupal\opigno_poll;

use Drupal\Core\Session\AccountInterface;

/**
 * Defines a common interface for opigno_poll vote controller classes.
 */
interface PollVoteStorageInterface {

  /**
   * Delete a user's votes for a opigno_poll choice.
   *
   * @param array $choices
   *   A list of choice ID's for each one we will remove all the votes.
   */
  public function deleteChoicesVotes(array $choices);

  /**
   * Delete a user's votes for an opigno_poll.
   *
   * @param PollInterface $opigno_poll
   *   The opigno_poll from which to delete the user's votes.
   *
   * @return mixed
   *   The result of the vote deletion operation.
   */
  public function deleteVotes(PollInterface $opigno_poll);

  /**
   * Cancel a user's vote.
   *
   * @param PollInterface $opigno_poll
   *   The opigno_poll from which to cancel the user's vote.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account whose vote will be canceled.
   */
  public function cancelVote(PollInterface $opigno_poll, AccountInterface $account = NULL);

  /**
   * Save a user's vote.
   *
   * @param array $options
   *   An array of options for saving the user's vote.
   */
  public function saveVote(array $options);

  /**
   * Get all votes for an opigno_poll.
   *
   * @param PollInterface $opigno_poll
   *   The opigno_poll for which to retrieve all votes.
   *
   * @return mixed
   *   The result of retrieving all votes for the opigno_poll.
   */
  public function getVotes(PollInterface $opigno_poll);

  /**
   * Get a user's votes for a opigno_poll.
   *
   * @param PollInterface $opigno_poll
   *   The opigno_poll for which to retrieve get user votes.
   *
   * @return array|false
   *   An array of the user's vote values, or false if the current user hasn't
   *   voted yet.
   */
  public function getUserVote(PollInterface $opigno_poll);

  /**
   * Get total votes for an opigno_poll.
   *
   * @param PollInterface $opigno_poll
   *   The opigno_poll for which to retrieve total votes.
   *
   * @return mixed
   *   The total number of votes for the opigno_poll.
   */
  public function getTotalVotes(PollInterface $opigno_poll);

}
