<?php

namespace Drupal\opigno_poll;

use Drupal\views\EntityViewsData;

/**
 * Render controller for opigno_polls.
 */
class PollViewData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['opigno_poll_field_data']['votes'] = [
      'title' => 'Total votes',
      'help' => 'Displays the total number of votes.',
      'real field' => 'id',
      'field' => [
        'id' => 'opigno_poll_totalvotes',
      ],
    ];

    $data['opigno_poll_field_data']['status_with_runtime'] = [
      'title' => 'Active with runtime',
      'help' => 'Displays the status with runtime.',
      'real field' => 'id',
      'field' => [
        'id' => 'opigno_poll_status',
      ],
    ];

    return $data;
  }

}
