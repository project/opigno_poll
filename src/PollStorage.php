<?php

namespace Drupal\opigno_poll;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;

/**
 * Controller class for opigno_polls.
 *
 * This extends the default content entity storage class,
 * adding required special handling for opigno_poll entities.
 */
class PollStorage extends SqlContentEntityStorage implements PollStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function getTotalVotes(PollInterface $opigno_poll) {
    return \Drupal::service('opigno_poll_vote.storage')->getTotalVotes($opigno_poll);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteVotes(PollInterface $opigno_poll) {
    return \Drupal::service('opigno_poll_vote.storage')->deleteVotes($opigno_poll);
  }

  /**
   * {@inheritdoc}
   */
  public function getUserVote(PollInterface $opigno_poll) {
    return \Drupal::service('opigno_poll_vote.storage')->getUserVote($opigno_poll);
  }

  /**
   * {@inheritdoc}
   */
  public function saveVote(array $options) {
    return \Drupal::service('opigno_poll_vote.storage')->saveVote($options);
  }

  /**
   * {@inheritdoc}
   */
  public function getVotes(PollInterface $opigno_poll) {
    return \Drupal::service('opigno_poll_vote.storage')->getVotes($opigno_poll);
  }

  /**
   * {@inheritdoc}
   */
  public function cancelVote(PollInterface $opigno_poll, AccountInterface $account = NULL) {
    \Drupal::service('opigno_poll_vote.storage')->cancelVote($opigno_poll, $account);
  }

  /**
   * {@inheritdoc}
   */
  public function getOpignoPollDuplicates(PollInterface $opigno_poll) {
    $query = \Drupal::entityQuery('opigno_poll');
    $query->accessCheck();
    $query->condition('question', $opigno_poll->label());

    if ($opigno_poll->id()) {
      $query->condition('id', $opigno_poll->id(), '<>');
    }
    return $this->loadMultiple($query->execute());
  }

  /**
   * {@inheritdoc}
   */
  public function getMostRecentopignopoll() {
    $query = \Drupal::entityQuery('opigno_poll')
      ->condition('status', opigno_poll_PUBLISHED)
      ->accessCheck(TRUE)
      ->sort('created', 'DESC')
      ->pager(1);
    return $this->loadMultiple($query->execute());
  }

  /**
   * {@inheritdoc}
   */
  public function getExpiredopignopolls() {
    $query = $this->database->query('SELECT id FROM {opigno_poll_field_data} WHERE (:timestamp > (created + runtime)) AND status = 1 AND runtime <> 0', [':timestamp' => \Drupal::time()->getCurrentTime()]);
    return $this->loadMultiple($query->fetchCol());
  }

}
