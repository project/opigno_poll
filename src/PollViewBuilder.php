<?php

namespace Drupal\opigno_poll;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Render controller for opigno_polls.
 */
class PollViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL) {
    $entity = $this->entityRepository->getTranslationFromContext($entity, $langcode);

    // Ajax request might send the view mode as a GET argument, use that
    // instead.
    if (\Drupal::request()->query->has('view_mode')) {
      $view_mode = \Drupal::request()->query->get('view_mode');
    }

    $output = parent::view($entity, $view_mode, $langcode);
    $output['#theme_wrappers'] = ['container'];
    $output['#attributes']['class'][] = 'opigno_poll-view';
    $output['#attributes']['class'][] = $view_mode;

    $output['#opigno_poll'] = $entity;
    $output['opigno_poll'] = [
      '#lazy_builder' => [
        'opigno_poll.post_render_cache:renderViewForm',
      [
        'id' => $entity->id(),
        'view_mode' => $view_mode,
        'langcode' => $entity->language()->getId(),
      ],
      ],
      '#create_placeholder' => TRUE,
      '#cache' => [
        'tags' => $entity->getCacheTags(),
      ],
    ];

    return $output;

  }

}
