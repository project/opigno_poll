<?php

namespace Drupal\opigno_poll;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines an access control handler for the opigno_poll entity.
 *
 * @see \Drupal\opigno_poll\Entity\Poll
 */
class PollAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ['create opigno_polls', 'administer opigno_polls'], 'OR');
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    if ($operation === 'view') {
      // Allow view access if the user has the access opigno_polls permission
      // or if anonymous voting is allowed.
      if ($entity->anonymous_vote_allow->value || $account->hasPermission('access opigno_polls')) {
        return AccessResult::allowed();
      }

      // Check if the user has any of the allowed roles specified for this poll.
      $user_roles = $account->getRoles();
      $allowed_roles = array_column($entity->field_allow_roles->getValue(), 'target_id');
      if (array_intersect($user_roles, $allowed_roles)) {
        return AccessResult::allowed();
      }
    }
    elseif ($operation === 'update' && !$account->isAnonymous() && $account->id() === $entity->getOwnerId()) {
      // Allow the owner of the poll to update it.
      return AccessResult::allowedIfHasPermissions($account, ['edit own opigno_polls', 'administer opigno_polls'], 'OR');
    }

    // Fallback to the parent method if access is not granted.
    return parent::checkAccess($entity, $operation, $account);
  }

  /**
   * {@inheritdoc}
   */
  protected function checkFieldAccess($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, FieldItemListInterface $items = NULL) {
    $restricted_fields = [
      'uid',
    ];
    if ($operation === 'edit' && in_array($field_definition->getName(), $restricted_fields, TRUE)) {
      return AccessResult::allowedIfHasPermission($account, 'administer opigno_polls');
    }
    return parent::checkFieldAccess($operation, $field_definition, $account, $items);
  }

}
