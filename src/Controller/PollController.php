<?php

namespace Drupal\opigno_poll\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\opigno_poll\PollInterface;

/**
 * Returns responses for opigno_poll module routes.
 */
class PollController extends ControllerBase {

  /**
   * Route title callback.
   *
   * @param \Drupal\opigno_poll\PollInterface $opigno_poll
   *   The opigno_poll entity.
   *
   * @return string
   *   The opigno_poll label.
   */
  public function opignoPollTitle(PollInterface $opigno_poll) {
    return $opigno_poll->label();
  }

}
