<?php

namespace Drupal\opigno_poll;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Defines a service for opigno_poll post render cache callbacks.
 */
class PollPostRenderCache implements TrustedCallbackInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new opigno_pollPostRenderCache object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks() {
    return ['renderViewForm'];
  }

  /**
   * Replaces placeholder with opigno_poll form in #post_render_cache callback.
   *
   * @param int $id
   *   The opigno_poll ID.
   * @param string $view_mode
   *   The view mode the opigno_poll should be rendered with.
   * @param string $langcode
   *   The langcode in which the opigno_poll should be rendered.
   *
   * @return array
   *   A renderable array containing the opigno_poll form.
   */
  public function renderViewForm($id, $view_mode, $langcode = NULL) {
    /**
* @var \Drupal\opigno_poll\PollInterface $opigno_poll
*/
    $opigno_poll = $this->entityTypeManager->getStorage('opigno_poll')->load($id);

    if ($opigno_poll) {
      if ($langcode && $opigno_poll->hasTranslation($langcode)) {
        $opigno_poll = $opigno_poll->getTranslation($langcode);
      }
      /**
* @var \Drupal\opigno_poll\Form\PollViewForm $form_object
*/
      $form_object = \Drupal::service('class_resolver')->getInstanceFromDefinition('Drupal\opigno_poll\Form\PollViewForm');
      $form_object->setOpignoPoll($opigno_poll);
      return \Drupal::formBuilder()->getForm($form_object, \Drupal::request(), $view_mode);
    }
    else {
      return ['#markup' => ''];
    }
  }

}
