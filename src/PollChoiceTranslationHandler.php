<?php

namespace Drupal\opigno_poll;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for opigno_poll.
 */
class PollChoiceTranslationHandler extends ContentTranslationHandler {

  /**
   * {@inheritdoc}
   */
  public function getFieldDefinitions() {
    return [];
  }

}
